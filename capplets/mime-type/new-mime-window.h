/* -*- Mode: C; tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 8 -*- */
/* Copyright (C) 1998 Redhat Software Inc. 
 * Authors: Jonathan Blandford <jrb@redhat.com>
 */
#include "mime-data.h"
#ifndef _NEW_MIME_WINDOW_H_
#define _NEW_MIME_WINDOW_H_

void launch_new_mime_window (void);
void hide_new_mime_window (void);
void show_new_mime_window (void);

#endif

